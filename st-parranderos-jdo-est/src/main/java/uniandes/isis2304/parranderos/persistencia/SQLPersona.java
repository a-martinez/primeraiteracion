package uniandes.isis2304.parranderos.persistencia;

import java.sql.Timestamp;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

;

public class SQLPersona {
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaLey100.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaLey100 pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLPersona (PersistenciaLey100 pp)
	{
		this.pp = pp;
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un TIPOBEBIDA a la base de datos de Parranderos
	 * @param pm - El manejador de persistencia
	 * @param idTipoBebida - El identificador del tipo de bebida
	 * @param nombre - El nombre del tipo de bebida
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarPersona (PersistenceManager pm, long numero_doc, String tipo_doc, String nombre, String apellido, Timestamp fecha, String correo, long id_eps) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaPersona  () + "(tipo_Doc, numero_Doc, nombre, apellido, fecha_Nacimiento, id_Eps, correo) values (?,?,?,?,?,?,?)");
        q.setParameters(tipo_doc, numero_doc,nombre,apellido,fecha,id_eps,correo);
        return (long) q.executeUnique();            
	}
}
