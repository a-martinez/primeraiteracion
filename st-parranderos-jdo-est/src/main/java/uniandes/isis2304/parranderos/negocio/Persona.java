
package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

public class Persona implements VOPersona
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	 private long numero_doc;
	 private String tipo_doc;
	 private String nombre;
	 private String apellido;
	 private Timestamp fecha;
	 private String correo;
	 private long id_eps;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor con valores
	 * @param id - El identificador del tipo de bebida
	 * @param nombre - El nombre del tipo de bebida
	 */


	public Persona(long numero_doc, String tipo_doc, String nombre, String apellido, Timestamp fecha, String correo,
			long id_eps) {
		super();
		this.numero_doc = numero_doc;
		this.tipo_doc = tipo_doc;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fecha = fecha;
		this.correo = correo;
		this.id_eps = id_eps;
	}


	@Override
	public String toString() {
		return "Persona [numero_doc=" + numero_doc + ", tipo_doc=" + tipo_doc + ", nombre=" + nombre + ", apellido="
				+ apellido + ", fecha=" + fecha + ", correo=" + correo + ", id_eps=" + id_eps + "]";
	}
	

	/**
	 * @param tipo - El TipoBebida a comparar
	 * @return True si tienen el mismo nombre
	 */
	public boolean equals(Object tipo) 
	{
		Persona tb = (Persona) tipo;
		return numero_doc == tb.numero_doc && tipo_doc.equalsIgnoreCase (tb.tipo_doc);
	}


	public long getNumero_doc() {
		return numero_doc;
	}


	public void setNumero_doc(long numero_doc) {
		this.numero_doc = numero_doc;
	}


	public String getTipo_doc() {
		return tipo_doc;
	}


	public void setTipo_doc(String tipo_doc) {
		this.tipo_doc = tipo_doc;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public Timestamp getFecha() {
		return fecha;
	}


	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}


	public String getCorreo() {
		return correo;
	}


	public void setCorreo(String correo) {
		this.correo = correo;
	}


	public long getId_eps() {
		return id_eps;
	}


	public void setId_eps(long id_eps) {
		this.id_eps = id_eps;
	}

	
}