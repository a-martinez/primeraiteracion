package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;

public interface VOPersona {
	
	public String toString();
	
	public boolean equals(Object tipo) ;

	public long getNumero_doc();

	public String getTipo_doc();


	public String getNombre();


	public String getApellido();


	public Timestamp getFecha();


	public String getCorreo();


	public long getId_eps();
}
