package uniandes.isis2304.parranderos.negocio;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import com.google.gson.JsonObject;

import uniandes.isis2304.parranderos.persistencia.PersistenciaLey100;
import uniandes.isis2304.parranderos.persistencia.PersistenciaParranderos;


public class Ley100
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(Ley100.class.getName());
	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia
	 */
	private PersistenciaLey100 pp;
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * El constructor por defecto
	 */
	public Ley100 ()
	{
		pp = PersistenciaLey100.getInstance ();
	}
	
	/**
	 * El constructor qye recibe los nombres de las tablas en tableConfig
	 * @param tableConfig - Objeto Json con los nombres de las tablas y de la unidad de persistencia
	 */
	public Ley100 (JsonObject tableConfig)
	{
		pp = PersistenciaLey100.getInstance (tableConfig);
	}
	
	/**
	 * Cierra la conexión con la base de datos (Unidad de persistencia)
	 */
	public void cerrarUnidadPersistencia ()
	{
		pp.cerrarUnidadPersistencia ();
	}
	
	/* ****************************************************************
	 * 			Métodos para manejar las PERSONAS
	 *****************************************************************/
	/**
	 * Adiciona de manera persistente un tipo de bebida 
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre del tipo de bebida
	 * @return El objeto TipoBebida adicionado. null si ocurre alguna Excepción
	 */
	public Persona adicionarPersona (long numero_doc, String tipo_doc, String nombre, String apellido, Timestamp fecha, String correo, long id_eps)
	{
        log.info ("Adicionando Persona: " + tipo_doc +": "+numero_doc);
        Persona persona = pp.adicionarPersona (numero_doc, tipo_doc, nombre, apellido, fecha, correo, id_eps);		
        log.info ("Adicionando Tipo de bebida: " + persona);
        return persona;
	}
	


}